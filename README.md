
## 🚀 Starting Meet-Test

- **[Postman](https://www.postman.com/alwayssoyoung/workspace/meet-test/overview).**

### Начало работы

Для запуска проекта.

```bash
./start
```

### Installation
1. **[DebugBar](https://github.com/barryvdh/laravel-debugbar)**
```bash
composer require barryvdh/laravel-debugbar --dev
```

2. **[Laravel Telescope](https://laravel.com/docs/9.x/telescope)**
```bash
composer require laravel/telescope

php artisan telescope:install

php artisan migrate
```

3. **Symlink** - для публичного доступа
```bash
php artisan storage:link
```






## API Reference

### Регистрация пользователя

```http
  POST /register
```

| Parameter | Type     | Description                | Example        |
|:----------| :------- | :------------------------- |----------------|
| `name` | `string` | **Required**. | Name           |
| `email` | `string` | **Required**. | test@email.com |
| `password` | `string` | **Required**. | test-password  |
| `password_confirmation` | `string` | **Required**. | test-password |
