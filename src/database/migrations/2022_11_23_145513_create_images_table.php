<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('imageable_id');
            $table->string('imageable_type');
            $table->string('filename');
            $table->string('path');
            $table->boolean('from_url')->default(false)->nullable();
            $table->string('disk')->default('public')->index()->nullable();
            $table->string('alt')->nullable();
            $table->string('title')->nullable();
            $table->string('scope')->index()->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('images');
    }
};
