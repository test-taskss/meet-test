<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Image extends Model
{
    use HasFactory;

    protected $attributes = [
        'url' => false,
    ];

    protected $casts = [
        'url' => 'string',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'imageable_id');
    }

    /**
     * Получить ссылку на изображение.
     */
    protected function url(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value,
        );
    }

    /**
     * Условие для получения изображения по значению "scope"
     */
    public function scopeScope(Builder $query, $scope): Builder
    {
        return $query->where('scope', $scope);
    }
}
