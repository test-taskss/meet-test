<?php

namespace App\Models;

use App\Events\SetModelUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'birth_date' => 'datetime:Y-m-d',
    ];

    protected $dispatchesEvents = [
        'creating' => SetModelUuid::class,
    ];

    public function image(): HasOne
    {
        return $this->hasOne(Image::class, 'imageable_id');
    }


}
// 'phone' => 'required|numeric|min:8|max:11',
