<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class SetModelUuid
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(Model $model)
    {
        $model->uuid = Str::uuid();
    }
}
