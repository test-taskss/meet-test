<?php

namespace App\Http\Resources\Collection;

use App\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    public $collects = User::class;

    public function toArray($request): array
    {
        return [
          'data' => $this->collection,
        ];
    }
}
