<?php

namespace App\Http\Controllers;

use App\Http\Resources\Collection\UserCollection;
use App\Http\Resources\Resource\UserResource;
use App\Models\User;
use App\Traits\HttpResponses;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    use HttpResponses;

    public function __invoke()
    {
        // TODO: Implement __invoke() method.
    }

    public function index() // create collection
    {
        $users = User::all();
        return UserCollection::collection(User::paginate());
//        return $users;
//        $users = User::all();
//        return response()->json($users); // Заменить на коллекцию
    }

    public function store(Request $request): JsonResponse
    {
//        $users = User::all();
        dd($request);
//        $user = new User($request->all());

//        return $this->success('Пользователь создан.', 200, 'id пользователя: {$user}');
    }
}
